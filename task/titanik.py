import pandas as pd



def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    titanic_df = get_titatic_dataframe()

    medians = {}

    titles = ["Mr.", "Mrs.", "Miss."]

    for title in titles:
        median_age = titanic_df.loc[titanic_df['Name'].str.contains(title), 'Age'].median()
        medians[title] = round(median_age)
    
    missings = [(title, titanic_df[titanic_df['Name'].str.contains(title)]['Age'].isnull().sum(), median_age)
                  for title, median_age in medians.items()]


    return missings
